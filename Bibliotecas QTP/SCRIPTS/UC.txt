Option Explicit

'##############################################################
'#
'# Script de TELA 
'# Identifica��o: sap_tela_zloja
'# Procedimento: Preenchimento da tela zloja
'# Vers�o: 		 1.0
'#
'# Atualiza��es:
'# 20130320 - Eduardo Rufino
'#
'#
'##############################################################

'=>>>>>>>>>>>>>>>>  I N I C I A R    G R A V A C A O  a partir deste PONTO
'Declarar todas as vari�veis a serem utilizadas
Dim szVarName, objStepsEvi, intInteracao, objTest
Dim szSistema, szFuncionalidade, szCenario

szSistema = Environment.Value("szSys_Config_Sistema")
szFuncionalidade = ""
szCenario = Environment.Value("szSys_Config_Cenario")

'========================================================================================
'Setar a intera��o corrente do teste
'========================================================================================
intInteracao =  Environment.Value("CEN_INTERACAO")  

'========================================================================================
'Indicar ao script de use case qual execu��o do QC est� sendo executada
'========================================================================================
set objTest = objGlobalRunOfTest 

'----------------------------------------------------------------------------------------
'Chamada das telas
'----------------------------------------------------------------------------------------
	'Tela - 


'========================================================================================
'Inserir a verifica��o abaixo logo ap�s a chamada de uma tela, observe que ser� necess�rio criar uma variavel para cada chamada de tela
'========================================================================================
If intReturn = intSUCESS Then
	blnCheckTelaReturn = True
	vMudarStatusTsT objStepsEvi, "Passed"
Else
	blnCheckTelaReturn = False
	vMudarStatusTsT objStepsEvi, "Failed"
End If

