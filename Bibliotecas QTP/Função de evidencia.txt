'Cria arquivo word no diret�rio temp
'Argumento:	sDoc -> nome do documento
Public sDoc
Function CriarWord(sDoc)
	sDirTemp = CreateObject("WScript.Shell").ExpandEnvironmentStrings("%Temp%")
	sDateTimeName = AddZeros(day(now)) & AddZeros(month(now)) & year(now) & AddZeros(hour(now)) & AddZeros(minute(now)) & AddZeros(second(now))
	sName=sDirTemp & "\" & sDoc & "_ " & sDateTimeName & ".doc"

	Set wordapp = CreateObject("Word.Application")
	wordapp.visible = false
	wordapp.documents.Add
	call wordapp.ActiveDocument.SaveAs(sName,wdFormatDocument)
	CriarWord = sName
	wordapp.Quit
	Set wordapp = Nothing
End Function


'Insere uma imagem no documento word apontado, se n�o existir documento ele reporta o erro
'Parameters:	sDoc -> nome do Word
							 'sName-> Nome do Step
							'sMensaje-> Mensagem a adicionar antes da imagem
							'sEstado-> Pass,Fail ou Done

Function AgregaPic(sDoc,SName,sMensaje,sEstado)
	sDirTemp = CreateObject("WScript.Shell").ExpandEnvironmentStrings("%Temp%")
	sTempDoc =  sDoc 

	sDateTimeName = AddZeros(day(now)) & AddZeros(month(now)) & year(now) & AddZeros(hour(now)) & AddZeros(minute(now)) & AddZeros(second(now))
	sImagePath =sDirTemp & "\" & replace(SName,"-","") & sDateTimeName & ".png"

	Desktop.CaptureBitmap sImagePath

	sTestName = Environment.value("ActionName")

	Set FSO = CreateObject("Scripting.FileSystemObject")
	If not FSO.FileExists(sTempDoc) Then
		Reporter.ReportEvent sEstado,SName,sMensaje,sImagePath 
	else
		Set wordapp = CreateObject("Word.Application")
        wordapp.Documents.Open sTempDoc
		wordapp.visible = false

		Set oDoc = wordapp.ActiveDocument
		Set oRange = oDoc.content
		oRange.ParagraphFormat.Alignment = 0
		oRange.insertafter  sTestName & vbCrLf &  sMensaje 
		oRange.collapse(0)

		oRange.InlineShapes.AddPicture  sImagePath, False, true

		wordapp.ActiveDocument.Save 
		wordapp.Quit : set wordapp = nothing

		If sEstado = micFail then 
			If not QCUtil.CurrentRun is nothing Then
				Call UploadEvidenciaQC(sTempDoc)
			else 
				 sMensaje = sMensaje & "		Evidence file: " &  sTempDoc
			End If
		End If

		Reporter.ReportEvent sEstado,SName,sMensaje 

	End if

	Set f =CreateObject("Scripting.FileSystemObject")
	f.Deletefile(sImagePath)

End Function

'Fun��o que apaga o word e carrega Evidencia no QC
Function UploadEvidenciaQC(sTempDoc)
	If  QCUtil.CurrentRun is nothing Then exit function
	Set FSO = CreateObject("Scripting.FileSystemObject")
	If not FSO.FileExists(sTempDoc) Then Exit Function

	Call UploadFileToQC(sTempDoc)
	Set fDoc =CreateObject("Scripting.FileSystemObject")
	fDoc.Deletefile(sTempDoc)
End Function

'Funci�n para subir ficheros a QC
Function UploadFileToQC(FilePath)
    	Set FSO = CreateObject("Scripting.FileSystemObject")
		If not FSO.FileExists(FilePath) Then Exit Function

        Set ObjCurrentTest = QCUtil.CurrentRun.Attachments
        Set ObjAttch = ObjCurrentTest.AddItem(Null)
        ObjAttch.FileName = FilePath
        ObjAttch.Type = 1
        ObjAttch.Post
        ObjAttch.Refresh
    
End Function